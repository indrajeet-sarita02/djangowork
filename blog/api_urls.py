from django.urls import path, include
from .api_views import *

#from rest_framework.routers import DefaultRouter, SimpleRouter

#router = DefaultRouter()
#router.register('blog', BlogViewSet)

urlpatterns = [
    path('blog/', BlogListView.as_view()),
    path('blog/<int:id>/', BlogListView.as_view())
    #path('blog', include(router.urls)),
    # path('poll/', PollAPIView.as_view()),
    # path('poll/<int:id>/', PollDetailView.as_view()),
    # path('generics/poll/', poll_list_view),
    # path('generics/poll/<int:id>/', PollListView.as_view()),
    # path('poll/search/', QuestionSearchViewSet.as_view({'get': 'list'})),
]