from django.shortcuts import render, reverse, redirect, get_object_or_404
from django.views import generic
from django.views.generic import View
from .models import Post
from .forms import PostForm

class PostList(generic.ListView):
    queryset = Post.objects.filter(status=1).order_by('-created_on')
    template_name = "blog/index.html"

class PostDetail(generic.DetailView):
    model = Post
    template_name = "blog/detail.html"

class PostView(View):

    def get(self, request, id=None):
        if id:
            post = get_object_or_404(Post, id=id)
            post_form = PostForm(instance=post)
            template = 'blog/edit_post.html'
        else:
            post_form = PostForm(instance=Post())
            template = 'blog/new_post.html'
        context = {'post_form': post_form}
        return render(request, template, context)

    def post(self, request, id=None):
        context = {}
        if id:
            return self.put(request, id)
            #post_form = PostForm(request.POST, instance=Post())
        post_form = PostForm(request.POST, instance=Post())
        if post_form.is_valid():
            new_post = post_form.save(commit=False)
            new_post.created_by = request.user
            new_post.save()
            return redirect('post_list')
        context = {'post_form': post_form}
        return render(request, 'blog/new_poll.html', context)

    def put(self, request, id=None):
        context = {}
        post = get_object_or_404(Post, id=id)
        post_form = PostForm(request.POST, instance=post)
        if post_form.is_valid():
            new_poll = post_form.save(commit=False)
            new_poll.created_by = request.user
            new_poll.save()
            return redirect('post_list')
        context = {'poll_form': post_form,}
        return render(request, 'post/edit_post.html', context)

    def delete(self, request, id=None):
        post= get_object_or_404(Post)
        post.delete()
        return redirect('blog_list')
