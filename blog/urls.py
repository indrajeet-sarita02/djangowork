from .views import *
from django.urls import path

urlpatterns = [
    path('add/', PostView.as_view(), name='post_add'),
    #path('list/', PostList.as_view(), name='home'),
    path('', PostList.as_view(), name='home'),
    path('blog/detail/<slug:slug>/', PostDetail.as_view(), name='post_detail'),
]